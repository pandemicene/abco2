# Alberta CO2 Tracking

## Initiative to collect CO2 data in indoor spaces across Alberta

Use the following format to name files

CO2monitor_StartDate(YYYYMMDDhhmmss)_EndDate(YYYYMMDDhhmmss)_Location.csv

Use local (Mountain Time) 

ex: Aranet4_20201017151310_20201020183043_WesternHighSchoolCalgary.csv

Have the first line in your csv file be the column names for the data therein

Avoid data with commas and quotes to reduce issues with reading

